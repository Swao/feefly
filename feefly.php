<?php include "base/db_connection.php"; ?>
<?php include "includes/script/userConnect.php"; ?>
<?php include "base/feefly.php"; ?>
<?php include "includes/global/head.php"; ?>

<link rel="stylesheet" type="text/css" href="assets/css/jquery.convform.css">
<link rel="stylesheet" href="assets/css/bootstrap.css">
<script type="text/javascript" src="assets/js/jquery.convform.js"></script>
<script type="text/javascript" src="assets/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="assets/js/custom.js"></script>


</head>
<body>
 
   <!-- ======= Mobile nav toggle button ======= -->
   <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

<!-- 
<div class="chat_icon">
  <i class="fa fa-comments" aria-hidden="true"></i>
</div>

<div class="chat_box">
  <div class="my-conv-form-wrapper">
    <form action="" method="GET" class="hidden">

      <select data-conv-question="Hello! How can I help you" name="category">
        <option value="WebDevelopment">Website Development ?</option>
        <option value="DigitalMarketing">Digital Marketing ?</option>
      </select>

      <div data-conv-fork="category">
        <div data-conv-case="WebDevelopment">
          <input type="text" name="domainName" data-conv-question="Please, tell me your domain name">
        </div>
        <div data-conv-case="DigitalMarketing" data-conv-fork="first-question2">
          <input type="text" name="companyName" data-conv-question="Please, enter your company name">
        </div>
      </div>

      <input type="text" name="name" data-conv-question="Please, Enter your name">

      <input type="text" data-conv-question="Hi {name}, <br> It's a pleasure to meet you." data-no-answer="true">

      <input data-conv-question="Enter your e-mail"
        data-pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
        type="email" name="email" required placeholder="What's your e-mail?">

      <select data-conv-question="Please Conform">
        <option value="Yes">Conform</option>
      </select>

    </form>
  </div>
</div>
 -->

<!-- ======= Header ======= -->
<header id="header">
  <div class="d-flex flex-column">

    <div class="profile">
      <img src="assets/img/avatar.png" alt="" class="img-fluid rounded-circle">
      <h1 class="text-light"><a href="#"><?php echo $profilResult['pseudo'] ?></a></h1>
    </div>

    <nav class="nav-menu">
      <ul>
        <li class="active"><a href="feefly.php"><i class="bx bx-home"></i> <span>Accueil</span></a></li>
        <li><a href="#services"><i class="bx bx-window"></i> Services</a></li>
        <li><a href="#portfolio"><i class="bx bx-window"></i> Catégories</a></li>
        <?php
            for ($i=0; $i < count($newtheme); $i++) { 
                ?>
                    <li><a href="#<?php echo utf8_encode($newtheme[$i]['name']); ?>"><i class="bx bx-window"></i> <?php echo utf8_encode($newtheme[$i]['name']); ?></a></li>
                <?php
            }
        ?>
      </ul>
      <button class="but">➕</button>
      <form action="feefly.php" method="POST">
        <div class="form-group">
          <label for="exampleInputEmail1"><a style="color:white;">Link</a></label>
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="theme" placeholder="catégorie"></br>
          <input type="hidden" name="addTheme" value="1">
          <input id="btn-submit" onclick="createtheme(<?php echo $profilResult['id'] ?>)" class="login100-form-btn" type="submit" name="ajouter" value="Valider"></input>          
        </div>
      </form>  
    </nav>
    <!-- .nav-menu -->
    <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

  </div>
  <img src="assets/img/feefly.png" class="logoFeedly" alt="IMG" width="200px">
</header>
<!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
  <div class="hero-container" data-aos="fade-in">
    <h1>Bienvenue <?php echo $profilResult['pseudo'] ?></h1>
    <p>Je suis <span class="typed" data-typed-items="un agrégateur, Feefly."></span></p>
  </div>
</section>
<!-- End Hero -->

<main id="main">

  <!-- ======= Services Section ======= -->
  <section id="services" class="services">
    <div class="container">

      <div class="section-title">
        <h2>Services</h2>
        <p>Feefly vous permet de suivre en direct 24/24h 7/7j vos actualités préférés de différents sites sur un même
          site grâce au flux RSS.</p>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up">
          <div class="icon"><i class="icofont-clock-time"></i></div>
          <h4 class="title"><a href="">Gagnez du temps</a></h4>
          <p class="description">Grâce à Feefly, vous allez gagner énormément de temps sans vous rendre compte</p>
        </div>
        <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
          <div class="icon"><i class="icofont-sand-clock"></i></div>
          <h4 class="title"><a href="">Trop rapide ?</a></h4>
          <p class="description">Vos articles s'actualise trop rapidement ? Laissez Feefly vous proposez les articles
            qui vous faut en fonctions de vos préférences grâce au mode lent</p>
        </div>
        <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
          <div class="icon"><i class="icofont-settings"></i></div>
          <h4 class="title"><a href="">Réglages</a></h4>
          <p class="description">En fonction de vos choix, réglez facilement votre interface. </p>
        </div>

      </div>

    </div>
  </section><!-- End Services Section -->

  <!-- ======= Portfolio Section ======= -->
  <section id="portfolio" class="portfolio section-bg">
    <div class="container">

      <div class="section-title">
        <h2>aggrégateur</h2>
        <p>Vous pouvez ajouter tous les aggrégateur que vous souhaitez.</p>
        <div class="section-title">
          <div class="row" data-aos="fade-up">
            <div class="col-lg-12 d-flex justify-content-center">
              <ul id="portfolio-flters">
                <li data-filter="">➕</li>
                <div class="wrapper">
                  <div class="input-data">
                    <input type="text" required>
                    <div class="underline">
                    </div>
                    <label>Title</label>
                  </div>
                </div>
                <div class="wrapper">
                  <div class="input-data">
                    <input type="text" required>
                    <div class="underline">
                    </div>
                    <label>Link</label>
                  </div><br>
                  <button type="button" class="btn btn-primary">Submit</button> 
                </div>
                |
                <!-- <li id="theme" data-filter="*" class="filter-active">All</li> -->
                <!--futur liste theme-->
                <li data-filter=".filter-app">App</li>
                <li data-filter=".filter-card">Card</li>
                <li data-filter=".filter-web">Web</li>
                <li data-filter=".filter-truc">test</li>
              </ul>
            </div>
          </div>
        </div> 
      </div>
      <div id="article" class="row portfolio-container" data-aos="fade-up" data-aos-delay="100">
        <!--future list article-->


        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox"
                title="App 1">
                <i class="bx bx-trash"></i></a>

              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>


        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox"
                title="Web 3"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-3.jpg" data-gall="portfolioGallery" class="venobox"
                title="App 2"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox"
                title="Card 2"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox"
                title="Web 2"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-6.jpg" data-gall="portfolioGallery" class="venobox"
                title="App 3"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-7.jpg" data-gall="portfolioGallery" class="venobox"
                title="Card 1"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-8.jpg" data-gall="portfolioGallery" class="venobox"
                title="Card 3"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox"
                title="Web 3"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-truc">
          <div class="portfolio-wrap">
            <img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
            <div class="portfolio-links">
              <a href="assets/img/portfolio/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox"
                title="Web 3"><i class="bx bx-trash"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Portfolio Section -->
 

      <!-- ======= Footer ======= -->
      <script src="assets/js/main.js"></script>
      <!-- End  Footer -->

      <?php include "includes/script/script.php"; ?>


</body>

</html>S